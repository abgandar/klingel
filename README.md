Klingel
=======

The purpose of this program is to allow building an intercom system from a
Raspberry Pi or Beaglebone like computer.

The setup is as follows:
 1. A microphone and a speaker are connected to the RPi via a simple USB sound card.
    They are installed (via e.g. telephone cables) at the door.
 2. A door bell button is connected to the RPi via a GPIO pin.
 3. Two relays are attached to the RPi via GPIO pins to drive an external bell
    and a door opener.

When the bell button is pushed, the RPi will ring the internal bell and start
a SIP call to a predefined number. When the call connects, the door speaker and 
mic are connected to the caller.

Via a secret key code, the caller can then trigger a signal to open the door
remotely.

The door opener can also be activated by writing the secret code to a local
FIFO socket in the file system. This can be used e.g. by a CGI script to open
the door remotely via a web service. An implementation of the HTML (suitable for
Android/iOS) and CGI are included.


Contributions:
 * Bell icon by Babasse (Sebastien Durel)
   Used under Creative Commons Attribution-Noncommercial-No Derivate Works 3.0 Unported
   http://icon-icons.com/de/symbol/Alarm-Glocke/20783
 * SIP call library PJSIP 2.5.5
   Used under GPL v2
   http://www.pjsip.org/

/*
 * Copyright (C) 2016-2017 Alexander Wittig <alexander@wittig.name>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 */

/**
 * klingel-cgi.c
 *
 *  CGI program passing query string to door opener socket.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include "config.h"

// Main program entry point
int main( int argc, char *argv[] )
{
    const char *arg = getenv( "QUERY_STRING" );
    if( arg == NULL )
    {
        puts( "Status: 400 Bad Request" );
        puts( "Content-type: application/json\n" );
        puts( "{\"status\":400,\"message\":\"Request incomplete\"}" );
        return 400;
    }

    int fd = open( FIFO_PFAD, O_WRONLY );
    if( fd == -1 )
    {
        puts( "Status: 500 Internal Server Error" );
        puts( "Content-type: application/json\n" );
        puts( "{\"status\":500,\"message\":\"Request failed\"}" );
        return 500;
    }
    write( fd, arg, strlen( arg ) );
    close( fd );

    puts( "Content-type: application/json\n" );
    puts( "{\"status\":200,\"message\":\"Request accepted\"}" );

    return 0;
}

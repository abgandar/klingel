//#define DEBUG                                   // Show very lengthy debug output


// SIP Settings
#define SIP_DOMAIN      "fritz.box"             // SIP Domain to register with (Fritzbox)
#define SIP_USER        "62X"                   // Username (from Fritzbox)
#define SIP_PASSWD      "password"              // Password (from Fritzbox)
#define SIP_NAME        "Türklingel"            // Display name
#define MAX_RING_TIME   30000                   // Max ring time (milliseconds) (optional)
#define MAX_CALL_TIME   60000                   // Max call time (milliseconds) (optional)

// Schedule and Numbers
#define NACHTRUHE_START 2200                    // Start of night time (no bell, alternative SIP URL)
#define NACHTRUHE_ENDE  800                     // Ende of night time (Format: hhmm with hh=24h WITHOUT leading zero (!) and mm = double digit minutes with leading zero)
#define SIP_TAG_URI     "sip:**755@fritz.box"   // SIP URI to call during day
#define SIP_NACHT_URI   "sip:**756@fritz.box"   // SIP URI to call during night

// Volume
#define VOL_SPEAKER     7.5                     // Volume speaker
#define VOL_MIC         2.0                     // Volume microphone

// Code and rate limit
#define TUERCODE        "1234567890"            // Secret door opener code
#define DTMF_MAX        20                      // max length of door opener code
#define RATE_LIMIT      120000                  // time frame for attempts (milliseconds)
#define RATE_MAX        5                       // max number of attempts in time frame

// durations
#define TUER_LOOPS      1                       // cycles door opener
#define TUER_DAUER      2000                    // duration of (half) cycle (milliseconds)
#define GLOCKE_LOOPS    1                       // cycles for bell
#define GLOCKE_DAUER    333                     // duration of cycle (milliseconds)
#define KLINGEL_DAUER   100                     // bell button delay (milliseconds)

// Pin Definitions
//#define KLINGEL_PIN     3                       // Pin for bell button (Beaglebone Black)
//#define TUER_PIN        50                      // Pin for door opener (Beaglebone Black)
//#define GLOCKE_PIN      51                      // Pin for door bell (Beaglebone Black)
#define KLINGEL_PIN     22                      // Pin for bell button (Pi zero)
#define TUER_PIN        17                      // Pin for door opener (Pi zero)
#define GLOCKE_PIN      27                      // Pin for door bell (Pi zero)

// Security
#define UNPRIV_USER     "klingel"               // Unpriviliged user
                                                // Must be in audio group
#define FIFO_PFAD       "/var/run/klingel"      // fifo socket for door opener

// UDPv6
//#define WITH_UDP6                               // UDPv6 instead of UDP

// Optional TLS
//#define WITH_TLS                                // TLS instead of UDP
//#define TLS_CERT_FILE           "cert.pem"      // TLS certificate file
//#define TLS_PRIVKEY_FILE        "key.pem"       // TLS private key file
//#define TLS_PRIVKEY_PASSWORD    "geheim"        // TLS private key password
